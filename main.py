import os
from time import ctime,localtime,strftime
import sys
import logging
import requests
from jinja2 import Environment, FileSystemLoader
from google.cloud import storage

OW_API_URL = "https://api.openweathermap.org"
TIMEOUT = 10

def get_coordinates(api_key, city_name) -> dict:
    """
    Get coordinates for a given city name
    :param api_key: OpenWeather API key
    :param city_name: City name
    :return: Dictionary with latitude and longitude
    """
    logging.info("Getting coordinates for %s", city_name)
    r_url = f"{OW_API_URL}/geo/1.0/direct?q={city_name}&appid={api_key}"
    response = requests.get(r_url, timeout=TIMEOUT)

    return {"lat": response.json()[0]["lat"], "lon": response.json()[0]["lon"]}

def get_data(api_key, pos) -> dict:
    """
    Get weather data for a given position
    :param api_key: OpenWeather API key
    :param pos: Dictionary with latitude and longitude
    :return: Dictionary with weather data
    """
    lat = pos["lat"]
    lon = pos["lon"]
    r_url = f"{OW_API_URL}/data/3.0/onecall?lat={lat}&lon={lon}&units=metric&appid={api_key}"
    response = requests.get(r_url, timeout=TIMEOUT)
    response_json = response.json()["current"]

    return {
        "lat": round(lat, 2),
        "lon": round(lon, 2),
        "temp": response_json["temp"],
        "desc": response_json["weather"][0]["description"],
        "feels like": response_json["feels_like"],
        "pressure": response_json["pressure"],
        "humidity": response_json["humidity"],
        "uvi": response_json["uvi"],
        "wind speed": response_json["wind_speed"],
    }

def generate_html(now, data, env_flag) -> str:
    """
    Generate index.html from template
    :param now: Current time
    :param data: Dictionary with weather data
    :param env_flag: Environment flag
    """
    file_loader = FileSystemLoader('templates')
    env = Environment(loader=file_loader)
    template = env.get_template('index.html')

    return template.render(updated_at=now, weather_data=data, env=env_flag)

def upload_to_s3(ts, bucket_name, index_html, data):
    """
    Upload data.json and index.html to S3 bucket
    :param ts: Timestamp
    :param bucket_name: S3 bucket name
    :param data: Dictionary with weather data
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)

    blob = bucket.blob(f'{ts}/data.json')
    blob.upload_from_string(str(data))
    logging.info("Uploaded data.json to %s/%s/data.json", bucket_name, ts)

    blob = bucket.blob('index.html')
    blob.cache_control = "no-cache"
    blob.upload_from_string(index_html, content_type="text/html")

    logging.info("Uploaded index.html to %s/index.html", bucket_name)

def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    now = ctime()
    now_ts = strftime("%Y-%m-%d-%H:%M:%S", localtime())

    city_filter = os.environ.get("CITY_FILTER", "Prague,Brno")
    bucket_name = os.environ.get("BUCKET_NAME") # required
    ow_api_key = os.environ.get("OW_API_KEY") # required
    env_flag = os.environ.get("ENV", "production")


    cities = city_filter.split(",")
    logging.info("Getting data for %s", cities)

    weather_data = {}
    for city in cities:
        pos = get_coordinates(ow_api_key, city)
        weather_data[city] = get_data(ow_api_key, pos)

    index_html = generate_html(now, weather_data, env_flag)
    logging.info("Generated index.html")

    upload_to_s3(now_ts, bucket_name, index_html, weather_data)

if __name__ == "__main__":
    main()
