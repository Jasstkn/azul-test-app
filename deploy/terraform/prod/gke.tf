module "gcp-network" {
  source  = "terraform-google-modules/network/google"
  version = ">= 4.0.1"

  project_id   = data.google_project.current.project_id
  network_name = local.network_name

  subnets = [
    {
      subnet_name   = local.subnet_name
      subnet_ip     = "10.0.0.0/17"
      subnet_region = local.region
    },
    {
      subnet_name   = local.master_auth_subnetwork
      subnet_ip     = "10.60.0.0/17"
      subnet_region = local.region
    },
  ]

  secondary_ranges = {
    (local.subnet_name) = [
      {
        range_name    = local.pods_range_name
        ip_cidr_range = "192.168.0.0/18"
      },
      {
        range_name    = local.svc_range_name
        ip_cidr_range = "192.168.64.0/18"
      },
    ]
  }
}

locals {
  prefix                 = "autopilot-test"
  cluster_type           = local.prefix
  network_name           = "${local.prefix}-network"
  subnet_name            = "${local.prefix}-subnet"
  master_auth_subnetwork = "${local.prefix}-master-subnet"
  pods_range_name        = "ip-range-pods-${local.prefix}"
  svc_range_name         = "ip-range-svc-${local.prefix}"
  subnet_names           = [for subnet_self_link in module.gcp-network.subnets_self_links : split("/", subnet_self_link)[length(split("/", subnet_self_link)) - 1]]
}

data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

module "gke" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/beta-autopilot-public-cluster/"
  version = "~> 26.1.1"

  project_id                      = data.google_project.current.project_id
  name                            = "${local.cluster_type}-cluster"
  regional                        = true
  region                          = local.region
  network                         = module.gcp-network.network_name
  subnetwork                      = local.subnet_names[index(module.gcp-network.subnets_names, local.subnet_name)]
  ip_range_pods                   = local.pods_range_name
  ip_range_services               = local.svc_range_name
  release_channel                 = "RAPID"
  enable_vertical_pod_autoscaling = true
  network_tags                    = [local.cluster_type]

  grant_registry_access = true
}
