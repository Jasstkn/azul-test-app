module "devops_test_app_workload_identity" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  version = "~> 26.1.1"

  use_existing_k8s_sa = true
  annotate_k8s_sa     = false
  name                = "devops-test-app"
  namespace           = "devops-test-app"
  project_id          = data.google_project.current.project_id
  roles               = ["roles/storage.admin", "roles/iam.workloadIdentityUser"]
}
