resource "random_id" "bucket_prefix" {
  byte_length = 8
}

resource "google_storage_bucket" "this" {
  name          = "${random_id.bucket_prefix.hex}-${var.env}-weather.mariakot.xyz"
  location      = "EU"
  force_destroy = true

  uniform_bucket_level_access = true

  website {
    main_page_suffix = "index.html"
  }

  versioning {
    enabled = true
  }
}

resource "google_storage_bucket_iam_member" "this" {
  bucket = google_storage_bucket.this.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}
