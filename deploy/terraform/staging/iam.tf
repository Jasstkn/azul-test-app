# must be moved to bootstrap
# Cloud Run Invoker Service Account
# Cloud Run Invoker Service Account
resource "google_service_account" "cloud_run_invoker_sa" {
  account_id   = "cloud-run-invoker"
  display_name = "Cloud Run Invoker"
}

# Project IAM binding
resource "google_project_iam_binding" "run_invoker_binding" {
  project = data.google_project.current.project_id

  role    = "roles/run.invoker"
  members = ["serviceAccount:${google_service_account.cloud_run_invoker_sa.email}"]
}

resource "google_project_iam_binding" "token_creator_binding" {
  project = data.google_project.current.project_id

  role    = "roles/iam.serviceAccountTokenCreator"
  members = ["serviceAccount:${google_service_account.cloud_run_invoker_sa.email}"]
}

# Cloud Run Job IAM binding
resource "google_cloud_run_v2_job_iam_binding" "binding" {
  project = data.google_project.current.project_id

  location = google_cloud_run_v2_job.devops_test_app.location
  name     = google_cloud_run_v2_job.devops_test_app.name
  role     = "roles/viewer"
  members  = ["serviceAccount:${google_service_account.cloud_run_invoker_sa.email}"]
}
