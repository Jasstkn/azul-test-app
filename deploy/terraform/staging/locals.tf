locals {
  app_name       = "devops-test-app"
  image_registry = "europe-docker.pkg.dev/${data.google_project.current.project_id}/${local.app_name}/${local.app_name}"

  cloud_run_region = "europe-west1"
}

