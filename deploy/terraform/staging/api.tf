# Enable Cloud Run API
resource "google_project_service" "cloudrun_api" {
  service            = "run.googleapis.com"
  disable_on_destroy = false
  project            = data.google_project.current.project_id
}

# Enable Compute Engine API
resource "google_project_service" "computeengine_api" {
  service            = "compute.googleapis.com"
  disable_on_destroy = false
  project            = data.google_project.current.project_id
}

# Enable Cloud Scheduler API
resource "google_project_service" "cloudscheduler_api" {
  service            = "cloudscheduler.googleapis.com"
  disable_on_destroy = false
  project            = data.google_project.current.project_id
}

# enable Artifact Registry API
resource "google_project_service" "artifactregistry_api" {
  service            = "artifactregistry.googleapis.com"
  disable_on_destroy = false
  project            = data.google_project.current.project_id
}
