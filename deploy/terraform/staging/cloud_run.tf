resource "google_cloud_run_v2_job" "devops_test_app" {
  name     = format("%s-%s", var.env, local.app_name)
  location = local.cloud_run_region

  template {
    task_count = 1
    template {
      containers {
        image = format("%s:%s", local.image_registry, var.image_tag)
        env {
          name  = "TZ"
          value = "Europe/Prague"
        }
        env {
          name  = "CITY_FILTER"
          value = "Prague,Brno,Ostrava,Plzen,Liberec,Olomouc"
        }

        env {
          name  = "BUCKET_NAME"
          value = google_storage_bucket.this.name
        }

        env {
          name  = "OW_API_KEY"
          value = var.open_weather_api_key
        }

        env {
          name  = "ENV"
          value = var.env
        }
      }
    }
  }


  lifecycle {
    ignore_changes = [
      launch_stage,
    ]
  }
}

resource "google_cloud_scheduler_job" "schedule_devops_test_app_job" {
  name             = "${var.env}-devops-test-app-job"
  description      = "Schedule devops test app job"
  schedule         = "0 * * * *"
  attempt_deadline = "320s"
  region           = local.cloud_run_region

  retry_config {
    retry_count = 1
  }

  http_target {
    http_method = "POST"
    uri         = "https://${google_cloud_run_v2_job.devops_test_app.location}-run.googleapis.com/apis/run.googleapis.com/v1/namespaces/${data.google_project.current.number}/jobs/${google_cloud_run_v2_job.devops_test_app.name}:run"

    oauth_token {
      service_account_email = google_service_account.cloud_run_invoker_sa.email
    }
  }
}
