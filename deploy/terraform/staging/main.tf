terraform {
  required_version = ">= 1.4.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.70.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.5.1"
    }
  }

  backend "gcs" {
    bucket = "terraform-double-zenith-391118"
    prefix = "terraform/staging"
  }
}

provider "google" {
  project = "double-zenith-391118"
  region  = "eu-central1"
}

data "google_project" "current" {}
