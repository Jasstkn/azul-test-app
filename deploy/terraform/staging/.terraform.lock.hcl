# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "4.70.0"
  constraints = "4.70.0"
  hashes = [
    "h1:3nPvQ/hhHjqiayBXLzY3THDxhx0u043bwDTTV0Y8w6k=",
    "zh:1b0366f8ec54e2d2bd201aba24bb8091a84159da666b0d794e9d97eb65a39b4c",
    "zh:3b8fdd08f6585b9075b0e6c1ab67e05264a83d3dafd75abba6a9e485adace7f1",
    "zh:530b49d8416446d03920aebc92b0f363a7d24827e8e31620bc18fac07781582c",
    "zh:54541a962891696199f068fb50b70cf79e68a9050dd72fd8af60b9ee6ea50d15",
    "zh:579a6a695151a7d6685fb5d5a900de6a64f8a7c3a80e7b535ce8374f9ec51922",
    "zh:5996f31420d7ee36b406875c9d77af15faa72334be28c731d9f5de3b7933426a",
    "zh:77b0878ddad0042c37c113db783595beca9aaa5e1ef8130cc37cca61237d8569",
    "zh:7a1d512a2e36464d7983b97d3a76d94c34ab772920d7e8a6fe243b95835d451b",
    "zh:965760a933a214ed86a0bc8a32a8614fef8b969a203137a2335387b455ec9bc4",
    "zh:cfc63c8792d88da3ae560748b565efc6328375a5bd4db460256500b73af51790",
    "zh:de0b33e995f66a0e2c74a28ee54fa22dcc156ac45070789000df05dd3533fbde",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "4.71.0"
  constraints = ">= 4.50.0, < 5.0.0"
  hashes = [
    "h1:ZODs+OEdcgGyn6U7ecLq9tNGRS2sQ6Al4F1zaNv1RqY=",
    "zh:03f2ef38d47d9bfdfc05b682694519af267c258bf48d4af44bee2eaf531d9165",
    "zh:2b6fd1c31e7bd1f0d5f4515b1bd10c51891e59393e4023fafc4d6c3f2a289547",
    "zh:7b45eb90538ef9061217ab056eb20481270b400bb44eb0960c669d86bb5bebfa",
    "zh:85a3df376a376078b36ac03f25dc6a41850f591ed9a22f6f1f613c3be210931f",
    "zh:8b709ea8cfb1b8323684c3d0ca077e87c6ee6764c8da4c8a6e33e38e01a9d04a",
    "zh:9bd0938dd6123805fcc913fa2f9462559380b9682b524437934095e11626c925",
    "zh:aa238f005d1b23cc5e50473cefb2057499285ec9b263256c0850143042dfc5b8",
    "zh:c2e6a7376de154d7391fe88fa34a211a73d4b09ea472f408e1e30da28da912d3",
    "zh:cf2dc4b96e28e45402f7d10673e98dfa42c4ea9caee6dbba6cc367a0a2c310c7",
    "zh:f2cf3d9d741057937764b6e1a9c5a4ee4b71fdb83e6a42a276c29715ed60dbb4",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:f5e2ade1550d1eff8c9a2958f1d9e47c91af277c73d420bf968e292c1b46d790",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.5.1"
  hashes = [
    "h1:IL9mSatmwov+e0+++YX2V6uel+dV6bn+fC/cnGDK3Ck=",
    "zh:04e3fbd610cb52c1017d282531364b9c53ef72b6bc533acb2a90671957324a64",
    "zh:119197103301ebaf7efb91df8f0b6e0dd31e6ff943d231af35ee1831c599188d",
    "zh:4d2b219d09abf3b1bb4df93d399ed156cadd61f44ad3baf5cf2954df2fba0831",
    "zh:6130bdde527587bbe2dcaa7150363e96dbc5250ea20154176d82bc69df5d4ce3",
    "zh:6cc326cd4000f724d3086ee05587e7710f032f94fc9af35e96a386a1c6f2214f",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b6d88e1d28cf2dfa24e9fdcc3efc77adcdc1c3c3b5c7ce503a423efbdd6de57b",
    "zh:ba74c592622ecbcef9dc2a4d81ed321c4e44cddf7da799faa324da9bf52a22b2",
    "zh:c7c5cde98fe4ef1143bd1b3ec5dc04baf0d4cc3ca2c5c7d40d17c0e9b2076865",
    "zh:dac4bad52c940cd0dfc27893507c1e92393846b024c5a9db159a93c534a3da03",
    "zh:de8febe2a2acd9ac454b844a4106ed295ae9520ef54dc8ed2faf29f12716b602",
    "zh:eab0d0495e7e711cca367f7d4df6e322e6c562fc52151ec931176115b83ed014",
  ]
}
