variable "image_tag" {
  type        = string
  description = "Image tag"
}

variable "open_weather_api_key" {
  type        = string
  sensitive   = true
  description = "Open weather API key"
}

variable "env" {
  type        = string
  description = "Environment: staging or production"
}
