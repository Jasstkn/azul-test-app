# must be moved to the bootstrap project
resource "google_artifact_registry_repository" "devops_test_app" {
  repository_id = local.app_name

  location    = "europe"
  description = "Devops test app docker repository"
  format      = "DOCKER"

  docker_config {
    immutable_tags = true
  }
}
