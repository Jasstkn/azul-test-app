# devops-test-app

Links:

🏗️ [staging](https://www.staging.weather.mariakot.xyz/)
🔥 [production](https://www.weather.mariakot.xyz/)

## Solution overview

### Cloud Run

The main idea is to utilize Cloud Run in a combination with Cloud Scheduler. The whole deployment is described via Terraform.

```mermaid
graph LR
  subgraph "Gitlab"
    repo["repo"] --> lint[lint] --> build[build artifacts] --> tfwf[tf workflow]
  end
  subgraph "DNS"
    domain["www.weather.mariakot.xyz"]
  end
  subgraph "GCP"
    subgraph "Cloud Storage"
      bucket["Bucket"]
    end
    subgraph "Cloud Run"
      job["Job"]
    end
    subgraph "Cloud Scheduler"
      schedule["Schedule"]
    end
    subgraph "LoadBalancer"
      lb["Google Application LoadBalancer"]
    end
  end
  tfwf -- update w/new tag --> job
  schedule -- trigger every hour --> job
  job --> bucket
  domain --> lb
  lb --> bucket
```

[Approximate cost calculation](https://cloud.google.com/products/calculator/#id=d3f7849d-b937-4373-bb73-849f81908584) per installation - 20.53 USD. Most services well below free tier limits. The biggest part is a public global LoadBalancer, which is 18.00 USD per month and it serves HTTPS & HTTP->HTTPS requests. Can be replaced with a cheaper alternative, e.g. Cloud CDN.

### GKE 

The main idea is to utilize Cloud Run in a combination with Cloud Scheduler. The whole deployment is described via Terraform.

```mermaid
graph LR
  subgraph "Gitlab"
    repo["repo"] --> lint[lint] --> build[build artifacts] --> tfwf[tf workflow] --> helm[deploy helm chart]
  end
  subgraph "DNS"
    domain["www.weather.mariakot.xyz"]
  end
  subgraph "GCP"
    subgraph "Cloud Storage"
      bucket["Bucket"]
    end
    subgraph "GKE"
      cj["CronJob"] --> job["Job"] --> pod["Pod"]
    end
    subgraph "LoadBalancer"
      lb["Public LB"]
    end
  end
  helm -- update w/new tag --> cj
  pod --> bucket
  domain --> lb
  lb --> bucket
```

[Approximate cost calculation](https://cloud.google.com/products/calculator/#id=d3f7849d-b937-4373-bb73-849f81908584). It's the same except of GKE cost. There is free tier that is cover the cost of 1 node. It's possible to combine preemptible nodes with regular nodes to reach the optimal cost/performance ratio.

Domain costs are not included in the calculation. It's possible to use Cloud DNS or external DNS provider. Automation for DNS records and certificats can be implemented via [external-dns](https://github.com/kubernetes-sigs/external-dns) and [cert-manager](https://cert-manager.io/).

## Design comparison

Cloud Run is a good fit for this application. It's a fully managed serverless solution that can scale up & down. It's also very cost-effective and easy to deploy. There are few downsides: lack of control over runtime and underlying infrastructure, not so flexible networking configuration, workload identity and secret management.

GKE can be used for the applications with higher requirements and more complicated scheduling logic. It's much more flexible and can be extended with custom components. You have full control over infrastructure and network with Classic GKE. It also allows to separate infrastructure and application code.

## Requirements

1. `GOOGLE_APPLICATION_CREDENTIALS` with the service account key must be set in the environment, type `File`
2. `OW_API_KEY` openweather API key must be set in the environment
3. `PROJECT_ID` GCP project ID must be set in the environment
4. REGISTRY must be set in the environment, e.g. `europe-docker.pkg.dev/${PROJECT_ID}/devops-test-app/devops-test-app`

For one of the stags [gcloud-helmfile](https://github.com/Jasstkn/gcloud-helmfile) image is being used. It contains number of external dependencies to speed up the pipeline. I'm the maintainer of this image and it's available on [Docker Hub](https://hub.docker.com/r/mariakot/gcloud-helmfile).

## CI/CD

There are 4 stages:

- init (calculate required variables, init terraform)
- lint (lint code & run tests)
- publish-artifacts (publish docker image, terraform plan, helm diff)
- deploy (apply terraform, deploy helm chart, rollback for tagged pipeline)

Deployment to "stage" includes only build image & terraform workflow. Runs on MRs, branches and `main` branch.

Deployment to "production" includes the whole pipeline as it contains infra code and helm chart deployment and rollback. It runs only on tags.

Manual steps introduced to deploy and rollback jobs.

## Nice to implement

### Fine-grained access control for bucket

Currently all objects in the bucket are available to `allUsers`. There are 2 ways to address this security concern: separate public and private files (e.g. raw data) into 2 different buckets or use fine-grained access and grant public access to this specific file. 

### Use GCP Secret Manager to store credentials

At the moment secrets are managed via Terraform variable with the setting `sensitive=true` to sanitize it in the terraform logs. The environment variable is visible anyone who has access to Cloud Run job deployed in the target GCP project. While it might be okay for testing purposes, this secret must be moved to GCP Secret Manager and consumed via Kubernetes `Secret` object to exclude its appearance in the Cloud Run's UI. 

### Improve CI/CD pipeline

- Pipeline must be re-usable, independent blocks can be moved in a separate repository and can be versioned and used via `include`
- Current checks print results only in job's logs. Using Gitlab's reports feature we can enrich pipelines and MRs with well-formatted reports to speed up review process and improve security.
- Continuous security checks is a good way to establish and ensure the required level of the security. Those checks can include code vunlerability & quality scanning, infrastructure static analysis, scanning of `Dockerfile` & built docker images.
- Introduce caching to speed up builds (e.g. image build, dependency pulling, good base images with pre-pulling/caching etc.)


### Resource order creation

In GCP some APIs must be enabled first as well as registry pre-populated for the component (chicken-egg problem). Can be solved via bootstrap module that executes a separate place as initial bootstrap of application.

### Application LoadBalancer configured manually

Due to convoluted load balancer creation process I decided to create it manually over UI to implement SSL termination. Previous design was directly pointing out to the google's s3 endpoint for serving static content.

### Different GCP projects

It's usually a good practice to separate different environments into different GCP projects. This way we can ensure that resources are not shared between environments and can be managed and billed independently. It can also helps to reduce the blast radius in case of the security breach.
