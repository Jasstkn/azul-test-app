import unittest

from main import generate_html

class TestGenerateSuccess_HTML(unittest.TestCase):
    def test_generate_html(self):
        self.maxDiff = None

        tests = [
            {
                "name": "Test production env",
                "now": "Wed Jun 28 22:27:33 2023", 
                "data": {
                    'Prague': {'lat': 50.09, 'lon': 14.42, 'temp': 17.16, 'desc': 'clear sky', 'feels like': 16.5, 'pressure': 1018, 'humidity': 60, 'uvi': 0, 'wind speed': 2.57}
                },
                "env": "production",
                "expected": "tests/production.html"
            },
            {
                "name": "Test staging env",
                "now": "Wed Jun 28 22:27:33 2023", 
                "data": {
                    'Prague': {'lat': 50.09, 'lon': 14.42, 'temp': 17.16, 'desc': 'clear sky', 'feels like': 16.5, 'pressure': 1018, 'humidity': 60, 'uvi': 0, 'wind speed': 2.57}
                },
                "env": "staging",
                "expected": "tests/staging.html"
            }
        ]
        for test in tests:
            with self.subTest(value=test["name"]):
                with open(test["expected"], "r") as f:
                    test["expected"] = f.read()
                self.assertEqual(
                    generate_html(
                    test["now"], test["data"], 
                    test["env"]), test["expected"])

if __name__ == '__main__':
    unittest.main()
