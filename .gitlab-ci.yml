---
stages:
- init
- lint
- publish-artifacts
- deploy

.runner_tags:
  tags:
    - docker

.terraform: &terraform
  - apt update && apt-get install -y jq
  - curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | bash
  - |
    if test -n "$CI_COMMIT_TAG"; then
      ENV="prod"
    # Else set the tag to the git commit sha
    else
      ENV="staging"
    fi
  - cd deploy/terraform/$ENV || exit
  - tfswitch

lint-py:
  image: python:3.7-slim-buster
  stage: lint
  extends: .runner_tags
  script:
    - pip install -r requirements.txt
    - pip install pylint -U
    - pylint *.py --exit-zero

test-py:
  image: python:3.7-slim-buster
  stage: lint
  extends: .runner_tags
  script:
    - pip install -r requirements.txt
    - python -m unittest discover

lint-docker:
  image: ghcr.io/hadolint/hadolint:latest-debian
  stage: lint
  script:
    - hadolint $CI_PROJECT_DIR/Dockerfile

image-get-tag:
  stage: init
  script:
    - |
      if test -n "$CI_COMMIT_TAG"; then
        tag="$CI_COMMIT_TAG"
      # Else set the tag to the git commit sha
      else
        tag="$CI_COMMIT_SHA"
      fi
    - echo "IMAGE_TAG=$tag" > build.env
  # parse tag to the build and merge jobs.
  # See: https://docs.gitlab.com/ee/ci/variables/#pass-an-environment-variable-to-another-job
  artifacts:
    reports:
      dotenv: build.env

push-image:
  stage: publish-artifacts
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [""]
  dependencies:
    - image-get-tag
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${REGISTRY_IMAGE}:${IMAGE_TAG}"

tf-init:
  stage: init
  before_script: *terraform
  script:
    - terraform init
  artifacts:
    paths:
      - deploy/terraform/*/.terraform.versions
      - deploy/terraform/*/.terraform
      - deploy/terraform/*/.terraform.lock.hcl
    expire_in: 1h

tf-validate:
  stage: lint
  before_script: *terraform
  script:
    - terraform validate
  needs:
    - tf-init

tf-lint:
  stage: lint
  before_script: *terraform
  script:
    - curl -s https://raw.githubusercontent.com/terraform-linters/tflint/master/install_linux.sh | bash
    - tflint --disable-rule=terraform_unused_declarations
  needs:
    - tf-init

tf-fmt:
  stage: lint
  before_script: *terraform
  script:
    - terraform fmt -check -recursive -diff
  needs:
    - tf-init

tf-plan:
  stage: publish-artifacts
  before_script: *terraform
  script:
    - terraform plan -compact-warnings -out "planfile" -var="image_tag=${IMAGE_TAG}" -var="open_weather_api_key=${OW_API_KEY}" -var="env=${ENV}"
    - terraform show --json planfile | jq -r "([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}" > "planfile_report"
  dependencies:
    - tf-init
    - image-get-tag
  artifacts:
    paths:
      - deploy/terraform/*/planfile

tf-apply:
  stage: deploy
  before_script: *terraform
  script:
    - terraform apply -compact-warnings "planfile"
  needs:
    - tf-init
    - tf-plan
  when: manual

.helm:
  image: mariarti/gcloud-helmfile:31defac2
  extends: .runner_tags
  dependencies:
    - image-get-tag
  rules:
  - if: $CI_COMMIT_TAG
  before_script:
    - gcloud auth activate-service-account --key-file=${GOOGLE_APPLICATION_CREDENTIALS} --project ${PROJECT_ID}
    - gcloud container clusters get-credentials autopilot-test-cluster --region=europe-west1

helm-diff:
  stage: publish-artifacts
  extends: .helm
  script:
    - helm diff --install devops-test-app deploy/charts/devops-test-app/ -n devops-test-app --set image.tag=${IMAGE_TAG} --set secretVars.OW_API_KEY=${OW_API_KEY}

helm-deploy:
  stage: deploy
  extends: .helm
  script:
    - helm upgrade --install devops-test-app deploy/charts/devops-test-app/ -n devops-test-app --create-namespace=true --set image.tag=${IMAGE_TAG} --set secretVars.OW_API_KEY=${OW_API_KEY}
  when: manual

helm-rollback:
  stage: deploy
  extends: .helm
  script:
    - helm rollback devops-test-app -n devops-test-app
  when: manual
